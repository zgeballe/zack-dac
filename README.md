# Code to model laser flash heating in diamond anvil cells

This repository hosts the code developed in
[Geballe, Sime, Badro, van Keken and Goncharov (2020)]
(https://www.sciencedirect.com/science/article/abs/pii/S0012821X20301047).


## How to use

- Make sure Docker is running.

- If using for first time, clone repository to your computer, in location of your choice:
```bash
git clone https://zgeballe@bitbucket.org/zgeballe/zack-dac.git
```

- Navigate to the directory:
```bash
cd zack-dac/
```

- Run the FEniCS docker container:
```bash
docker run -it -v $(pwd):/home/fenics/shared quay.io/natesime/dolfin_dg:master
```

- Install the PETScTS branch of dolfin_dg which is used for adaptive timestepping:
```bash
git clone --single-branch --branch nate/petsc4py-ts https://bitbucket.org/nate-sime/dolfin_dg.git
pip install dolfin_dg/ --user
```

- Install `pygmsh@6.0.2`, `h5py`, `gmsh@4.0.7` and their dependencies:
```bash
pip3 install pygmsh==6.0.2 h5py --user &&\
wget http://gmsh.info/bin/Linux/gmsh-4.0.7-Linux64.tgz &&\
tar -xvf gmsh-4.0.7-Linux64.tgz && rm gmsh-4.0.7-Linux64.tgz &&\
sudo mv gmsh-4.0.7-Linux64/bin/gmsh /usr/local/bin/ && rm -r gmsh-4.0.7-Linux64 &&\
sudo apt-get update &&\
sudo apt-get -y install python3-lxml &&\
sudo apt-get -y install libxcursor-dev &&\
sudo apt-get -y install libglu1 &&\
sudo apt-get -y install libxft-dev &&\
sudo apt-get -y install libxinerama-dev 
```

- Go to `modelling` folder which is shared with the Docker container in the `shared` directory:
```bash
cd shared/modelling/
```

- Run code. For example, to generate a mesh, run a FE solver, and plot the results in comparison to laboratory data:
```bash
python3 plot_heatT.py
```


## Contributors

* Zachary Geballe <zgeballe@carnegiescience.edu>
* Nate Sime <nsime@carnegiescience.edu>


## License

[MIT](https://opensource.org/licenses/MIT)