# Compute quantities of interest (qoi). E.g. measure the temperature at position x0 = (50km, -50km).
# We need to find on which process x0 resides before computing the interpolant of the FE function.
def evaluate_function(x, u):
    import dolfin as dfn
    import numpy as np
    comm = u.function_space().mesh().mpi_comm()
    if comm.size == 1:
        return np.array(list(map(u, x)), dtype=np.double)

    u_shape = u.value_shape()[0] if len(u.value_shape()) > 0 else 1
    local_measured = np.zeros((x.shape[0], u_shape))
    for j in range(x.shape[0]):
        x0 = Point(*x[j])
        u_cell, distance = u.function_space().mesh().bounding_box_tree().compute_closest_entity(x0)
        local_measured[j, :] = u(x0) if distance < dfn.DOLFIN_EPS else None

    gathered_u_vals = comm.gather(local_measured)

    u_vals = np.zeros((x.shape[0], u_shape))
    if comm.rank == 0:
        gathered_u_vals = np.hstack(gathered_u_vals)
        for j in range(gathered_u_vals.shape[0]):
            u_vals[j] = np.fromiter((y for y in gathered_u_vals[j] if not np.isnan(y)), dtype=np.double)

    if len(u_vals) == 1:
        u_vals = u_vals[0]
    return u_vals


def output_time_series_to_csv(fi, t, val, append=True):
    import dolfin as dfn
    if dfn.MPI.comm_world.rank == 0:
        with open(fi, "a" if append else "w") as fi:
            fi.write("%.6e,%.6e\n" % (float(t), val))
