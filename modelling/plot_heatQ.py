'''
Use this file to run and plot results of simulation using run_heatQ.py, or to plot results from a previously run simulation. 

Define simulation parameters in pp and pp_Q. Use 

Compare against laboratory data by defining pp.meas_fname, rT_file_p, and rT_file_o as the filenames with the laboratory data.

One tricky parameter is the Boolean "use_Q_from_heatT", which  allows you to use the flux from heatT simulations 
as the heating source in heatQ simulations. To use this option, you must have already generated a .json file with filename
equal to pp_Q.flux_fname.
'''

from run_heatQ import normalize_Ts, run_heatQ, all_heatQ_params, gaussian
from run_heatT import all_params, materials, SAMPLE, COUPLER, INSULATOR, GASKET, DIAMOND
from fns_for_plotting import *

run_simulation = True

pp = all_params(name2save = '80GPa',meas_fname = 'Pyro80GPa_29Jun2018_10us.txt', t0_Tfile = 0e-6, dt = 1e-11, time_f = 7.5e-6,\
                          rho = {SAMPLE: 5164.0, COUPLER: 26504.0, INSULATOR: 4131.0, GASKET: 21000.0, DIAMOND: 3500.0},\
                          cv = {SAMPLE: 1158.0, COUPLER: 130.0, INSULATOR: 668.0, GASKET: 140.0, DIAMOND: 510.0},\
                          kappa = {SAMPLE: 3.9, COUPLER: 352, INSULATOR: 10, GASKET: 100.0, DIAMOND: 2000.0},\
                          d_sam = 1.84e-6, d_coupler = 62e-9, d_ins_pside = 3.2e-6, d_ins_oside = 6.3e-6,\
                          r_sam = 20e-6, r_ins = 30e-6, r_diamond = 100e-6, d_diamond = 40e-6, T_FWHM = 13.2e-6,\
                          mesh_fname = 'diamond_mesh_2d_80GPa',sim_fname = 'guessed-fit_heatQ_80GPa',\
                          )

pp_Q = all_heatQ_params(p_FWHM = 10.8e-6, p0_pside = 4.86, p0_oside = 4.88, pscale_pulse = 1.9e4,\
                          gauss_params2add = [2e-6, 6e-6, 1e-6, 0.08, 0],use_Q_from_heatT=True, flux_fname = 'guessed-fit_heatT_80GPa',\
                          monitor_Planck_avgT = False)

rT_file_p = 'Pyro80GPa_29Jun2018_DS_021_OneColorTr.txt'
rT_file_o = 'Pyro80GPa_29Jun2018_US_022_OneColorTr.txt'

if run_simulation:
  rms,R2,tt,Tp_sim,Tp_sim_norm,To_sim,To_sim_norm,Tp_meas,Tp_meas_norm,\
  To_meas,To_meas_norm,r_top_coupler,final_T_top_coupler, initial_T_pside, initial_T_oside, QQ = run_heatQ(pp,pp_Q)
  tT_sim = (tt,Tp_sim,To_sim)
  tT_sim_norm = (tt,Tp_sim_norm,To_sim_norm)
  tT_meas = (tt,Tp_meas,To_meas)
  tT_meas_norm = (tt,Tp_meas_norm,To_meas_norm)
  tQ_sim = (tt,QQ)
  rTp_sim = (r_top_coupler,initial_T_pside)
  rTo_sim = (r_top_coupler,initial_T_oside)
else:
  print('Loading '+pp.sim_fname+'.json')
  kappa, tT_sim,tT_meas, tQ_sim,rTp_sim, rTo_sim = load_tT_json_heatT(pp)

plot_tT(tT_sim, pp, tT_meas=tT_meas, normalize_boolean = True, fig_name = 'tT_norm_heatQ_'+pp.name2save, fig_num = 1)
plot_tT(tT_sim, pp, tT_meas=tT_meas, normalize_boolean = False, fig_name = 'tT_heatQ_'+pp.name2save,fig_num = 2)
plot_tQ(tQ_sim, pp, tQ_meas=[], fig_name ='tQ_heatQ_'+pp.name2save,fig_num = 3)

plot_rT(rTp_sim, pp, rT_files = [rT_file_p,rT_file_o] , fig_name ='rT_heatQ_'+pp.name2save,fig_num = 4)
plot_rT(rTo_sim, pp, fig_name ='rT_heatQ_'+pp.name2save,fig_num = 4)



