'''
Use this file to create two dimensional meshes.
In order to vary the fineness of the mesh, the variables lcar, lcar_coupler, etc., should be changed inside the function make_diamond_mesh_2d.
In order to vary the spatial dimensions such as sample thickness, radius, etc., use the mesh_params variable (e.g. "pp") that is passed into make_diamond_mesh_2d. 

'''

import h5py #put this first
import pygmsh
import json
import meshio
import numpy as np

mesh_folder = '../meshing/'

class mesh_params:
    def __init__(self,d_sam, d_coupler,  d_ins_pside, d_ins_oside, r_sam, r_ins, r_diamond, d_diamond, mesh_fname):
        self.d_sam = d_sam
        self.d_coupler = d_coupler
        self.d_ins_pside = d_ins_pside
        self.d_ins_oside = d_ins_oside
        self.r_sam = r_sam
        self.r_coupler = r_sam #no need for difference
        self.r_ins = r_ins
        self.r_diamond = r_diamond
        self.d_diamond = d_diamond
        self.mesh_fname = mesh_fname


def make_diamond_mesh_2d(pp):
#    print(meshio.__version__)
    import h5py
#    print(h5py.__version__, h5py.__file__)

    mesh_fname = pp.mesh_fname
    d_sam = pp.d_sam*1e6
    d_coupler = pp.d_coupler*1e6
    d_ins_pside = pp.d_ins_pside*1e6
    d_ins_oside = pp.d_ins_oside*1e6
    r_sam = pp.r_sam*1e6
    r_ins = pp.r_ins*1e6
    r_diamond = pp.r_diamond*1e6
    d_diamond = pp.d_diamond*1e6

    r_coupler = r_sam
    
    ## Parameters used for the 80 GPa data in Geballe et al., EPSL 536 (2020) 116161. 
    lcar = 10.0
    lcar_coupler = lcar/(200.0)
    lcar_fine = lcar/10.0
    lcar_superfine = lcar/100.0

    geom = pygmsh.built_in.Geometry()

    p1 = geom.add_point((0.0, 0.0, 0.0), lcar_superfine)
    p2 = geom.add_point((r_sam, 0.0, 0.0), lcar_superfine)
    p3 = geom.add_point((r_ins, 0.0, 0.0), lcar_fine)
    p4 = geom.add_point((r_diamond, 0.0, 0.0), lcar)

    p5 = geom.add_point((0.0, d_sam/2, 0.0), lcar_coupler)
    p6 = geom.add_point((r_sam, d_sam/2, 0.0), lcar_coupler)
    p7 = geom.add_point((0.0, d_sam/2 + d_coupler, 0.0), lcar_coupler)
    p8 = geom.add_point((r_coupler, d_sam/2 + d_coupler, 0.0), lcar_coupler)

    y1 = d_sam/2+d_coupler+d_ins_pside
    p9 = geom.add_point((0.0, y1, 0.0), lcar_fine)
    p10 = geom.add_point((r_ins, y1, 0.0), lcar_fine)
    p11 = geom.add_point((r_diamond, y1, 0.0), lcar)

    y2 = y1 + d_diamond
    p12 = geom.add_point((0.0, y2, 0.0), lcar)
    p13 = geom.add_point((r_diamond, y2, 0.0), lcar)

    p14 = geom.add_point((0.0, -d_sam / 2, 0.0), lcar_coupler)
    p15 = geom.add_point((r_sam, -d_sam / 2, 0.0), lcar_coupler)
    p16 = geom.add_point((0.0, -(d_sam / 2 + d_coupler), 0.0), lcar_coupler)
    p17 = geom.add_point((r_coupler, -(d_sam / 2 + d_coupler), 0.0), lcar_coupler)

    y3 = - d_sam/2 - d_coupler - d_ins_oside
    p18 = geom.add_point((0.0, y3, 0.0), lcar_fine)
    p19 = geom.add_point((r_ins, y3, 0.0), lcar_fine)
    p20 = geom.add_point((r_diamond, y3, 0.0), lcar)

    y4 = y3-d_diamond
    p21 = geom.add_point((0.0, y4, 0.0), lcar)
    p22 = geom.add_point((r_diamond, y4, 0.0), lcar)

    l1 = geom.add_line(p1, p2)
    l2 = geom.add_line(p2, p3)
    l3 = geom.add_line(p3, p4)
    l4 = geom.add_line(p1, p5)

    l5 = geom.add_line(p2, p6)
    l6 = geom.add_line(p3, p10)
    l7 = geom.add_line(p4, p11)
    l8 = geom.add_line(p5, p6)

    l9 = geom.add_line(p5, p7)
    l10 = geom.add_line(p6, p8)
    l11 = geom.add_line(p7, p8)
    l12 = geom.add_line(p7, p9)

    l13 = geom.add_line(p9, p10)
    l14 = geom.add_line(p10, p11)
    l15 = geom.add_line(p9, p12)
    l16 = geom.add_line(p11, p13)

    l17 = geom.add_line(p12, p13)
    l18 = geom.add_line(p1, p14)
    l19 = geom.add_line(p2, p15)
    l20 = geom.add_line(p3, p19)

    l21 = geom.add_line(p4, p20)
    l22 = geom.add_line(p14, p15)
    l23 = geom.add_line(p14, p16)
    l24 = geom.add_line(p15, p17)

    l25 = geom.add_line(p16, p17)
    l26 = geom.add_line(p16, p18)
    l27 = geom.add_line(p18, p19)
    l28 = geom.add_line(p19, p20)

    l29 = geom.add_line(p18, p21)
    l30 = geom.add_line(p20, p22)
    l31 = geom.add_line(p21, p22)

    ll1 = geom.add_line_loop([l1, l5, -l8, -l4])
    ll2 = geom.add_line_loop([l8, l10, -l11, -l9])
    ll3 = geom.add_line_loop([l2, l6, -l13, -l12, l11, -l10, -l5])
    ll4 = geom.add_line_loop([l3, l7, -l14, -l6])
    ll5 = geom.add_line_loop([l13, l14, l16, -l17, -l15])

    ll6 = geom.add_line_loop([l1, l19, -l22, -l18])
    ll7 = geom.add_line_loop([l22, l24, -l25, -l23])
    ll8 = geom.add_line_loop([l2, l20, -l27, -l26, l25, -l24, -l19])
    ll9 = geom.add_line_loop([l3, l21, -l28, -l20])
    ll10 = geom.add_line_loop([l27, l28, l30, -l31, -l29])

    s1 = geom.add_plane_surface(ll1)
    s2 = geom.add_plane_surface(ll2)
    s3 = geom.add_plane_surface(ll3)
    s4 = geom.add_plane_surface(ll4)
    s5 = geom.add_plane_surface(ll5)
    s6 = geom.add_plane_surface(ll6)
    s7 = geom.add_plane_surface(ll7)
    s8 = geom.add_plane_surface(ll8)
    s9 = geom.add_plane_surface(ll9)
    s10 = geom.add_plane_surface(ll10)

    geom.add_physical_surface([s1, s6], "sample")
    geom.add_physical_surface([s2, s7], "coupler")
    geom.add_physical_surface([s3, s8], "insulator")
    geom.add_physical_surface([s4, s9], "gasket")
    geom.add_physical_surface([s5, s10], "diamond")

    # points, cells, point_data, cell_data, field_data = pygmsh.generate_mesh(geom, dim=2)
    with open("diamond_mesh_sym_reference_geo.txt", "w") as f:
        f.write(geom.get_code())
    
    pygmsh_mesh = pygmsh.generate_mesh(geom, dim=2, prune_z_0=True)
    meshio.write(mesh_folder+mesh_fname+".xdmf", pygmsh_mesh)
    print('Wrote '+mesh_folder+mesh_fname+'.xdmf, '+mesh_fname+'.h5')

    # For testing a mesh
    # import dolfin
    # mesh = dolfin.Mesh()
    # dolfin.XDMFFile("diamond_2d_80GPa.xdmf").read(mesh)
    # 
    # V = dolfin.FunctionSpace(mesh, "CG", 1)
    # u, v = dolfin.Function(V), dolfin.TestFunction(V)
    #
    # dolfin.solve(dolfin.dot(dolfin.grad(u), dolfin.grad(v))*dolfin.dx
    #              - dolfin.Constant(1.0)*v*dolfin.dx == 0, u,
    #              dolfin.DirichletBC(V, dolfin.Constant(0.0), "on_boundary"))
    # dolfin.plot(u)
    # import matplotlib.pyplot as plt
    # plt.show()

    with open(mesh_folder+mesh_fname+'.json', "w") as f:
        f.write(json.dumps({"d_sam": d_sam, "d_coupler":d_coupler, "d_ins_pside":d_ins_pside, "d_ins_oside":d_ins_oside, 
                            "d_diamond":d_diamond, "r_sam":r_sam, "r_coupler":r_coupler, "r_ins": r_ins, "r_diamond":r_diamond}))
    print('Wrote '+mesh_folder+mesh_fname+'.json')


if __name__ == "__main__":


    pp = mesh_params(d_sam = 1.84e-6, d_coupler = 62e-9, d_ins_pside = 4e-6, d_ins_oside = 4e-6,\
                     r_sam = 20e-6, r_ins = 30e-6, r_diamond = 100e-6, d_diamond = 40e-6,\
                     mesh_fname = 'test_mesh')

    make_diamond_mesh_2d(pp)
