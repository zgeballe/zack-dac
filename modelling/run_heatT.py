'''
 Use this file to run simulations via 'method T' described in Geballe et al., EPSL 536 (2020) 116161. 

 'Method T' is solves the heat equation with an internal boundary condition at the pulsed-heated side of a sample in a 
 diamond anvil cell. The internal boundary condition is a time-dependent Diriclet boundary condition. 
 There is no heating source in this method. This method is intended to be used to simulate normalized 
 temperature variations (see Fig. S5 of Geballe et al., 2020 for an example of normalization). The boundary condition
 at domain edges is T = 300 K. Axial symmetry is assumed, enabling 2-d simulation.   
'''

import h5py #Warning: this package must be imported prior to dolfin
from dolfin import *
from dolfin_dg import *
import numpy as np
import json
import utility
from run_mesh_2d import make_diamond_mesh_2d, mesh_params

### List materials 
SAMPLE = 1
COUPLER = 2
INSULATOR = 3
GASKET = 4
DIAMOND = 5
materials = (DIAMOND, GASKET, INSULATOR, COUPLER, SAMPLE)

### Define a class to store basic information about simulation and laboratory data
class all_params:
    def __init__(self, name2save,  meas_fname, t0_Tfile, dt, time_f, rho, cv, kappa,  T_FWHM,\
                     d_sam, d_coupler,  d_ins_pside, d_ins_oside, r_sam, r_ins, r_diamond, \
                     d_diamond,mesh_fname,sim_fname='sim', ksam4error_analysis = [],time_delay_oside_meas=0e-6,\
                     mesh_folder = '../meshing/', data_folder = '../data/', folder2save = '../sim_results/'):    
        self.name2save = name2save    
        self.meas_fname = meas_fname
        self.t0_Tfile = t0_Tfile
        self.dt = dt
        self.time_f = time_f
        self.rho = rho
        self.cv = cv
        self.kappa = kappa
        self.T_FWHM = T_FWHM
        self.d_sam = d_sam
        self.d_coupler = d_coupler
        self.d_ins_pside = d_ins_pside
        self.d_ins_oside = d_ins_oside
        self.r_sam = r_sam
        self.r_ins = r_ins
        self.r_diamond = r_diamond
        self.d_diamond = d_diamond
        self.mesh_fname = mesh_fname
        self.sim_fname = sim_fname
        if ksam4error_analysis == []:
            self.ksam4error_analysis = kappa[SAMPLE]
        else:
            self.ksam4error_analysis = ksam4error_analysis
        self.time_delay_oside_meas = time_delay_oside_meas
        self.mesh_folder = mesh_folder
        self.data_folder = data_folder
        self.folder2save = folder2save

### Normalize pulsed side and other side temperature-time function, as in Fig. S5 of Geballe et al. 2020 
def normalize_Ts(Tp,To):
    Tp = np.array(Tp)
    To = np.array(To)
    Tp -= np.min(Tp)
    To -= np.min(To)
    mag = np.max(Tp)
    Tp /= mag
    To /= mag
    return(Tp,To)

######################################
### Two functions for creating and loading meshes
def create_pp_mesh_from_pp(pp):
    pp_mesh = mesh_params(d_sam = pp.d_sam, d_coupler = pp.d_coupler, d_ins_pside = pp.d_ins_pside,\
                                d_ins_oside = pp.d_ins_oside, r_sam = pp.r_sam, r_ins = pp.r_ins,\
                                r_diamond = pp.r_diamond, d_diamond = pp.d_diamond, mesh_fname = pp.mesh_fname)
    return(pp_mesh)

def load_mesh(pp):
    xdmf_in = XDMFFile(pp.mesh_folder+pp.mesh_fname+'.xdmf')
    mesh = Mesh()
    xdmf_in.read(mesh)
    with open(pp.mesh_folder+pp.mesh_fname+'.json',"r") as f:
        mesh_info = json.load(f)
         #Convert to um
    mesh.coordinates()[:] *= 1e-6
    pp_mesh_loaded = mesh_params(d_sam = mesh_info["d_sam"]*1e-6, d_coupler = mesh_info["d_coupler"]*1e-6,\
                                         d_ins_pside = mesh_info["d_ins_pside"]*1e-6,d_ins_oside = mesh_info["d_ins_oside"]*1e-6,\
                                         r_sam = mesh_info["r_sam"]*1e-6, r_ins = mesh_info["r_ins"]*1e-6,\
                                         r_diamond = mesh_info["r_diamond"]*1e-6, d_diamond = mesh_info["d_diamond"]*1e-6,\
                                         mesh_fname = pp.mesh_fname)
    return(pp_mesh_loaded, mesh, xdmf_in)
#######################################
#######################################
### Statistics
def root_mean_square(a,b): 
    if len(a) != len(b):
        print('list lengths not equal for rms calculation')
    return(np.sum((np.array(a)-np.array(b))**2)/len(a))

def R_squared(meas,sim):
    sim = np.array(sim); meas = np.array(meas)
    SSres = len(sim)*root_mean_square(sim,meas)
    SStot = np.sum((meas-np.mean(meas))**2)
    return (1 - SSres/SStot)
########################################
########################################
### Run simulation via 'method T' described in Geballe et al. 2020

def run_heatT(pp):
    print('Using '+pp.mesh_fname+'.xdmf and .h5')
    t = Constant(0)  #Initializes time

### Define a Nonlinear problem to assemble the residual and Jacobian
    class Problem(NonlinearProblem):
        def __init__(self, a, L, bcs):
            self.assembler = SystemAssembler(a, L, bcs)
            NonlinearProblem.__init__(self)

        def F(self, b, x):
            self.assembler.assemble(b, x)

        def J(self, A, x):
            self.assembler.assemble(A)

### Use a Newton solver with custom damping parameter
    class CustomSolver(NewtonSolver):
        def __init__(self):
            NewtonSolver.__init__(self, mesh.mpi_comm(),
                              PETScKrylovSolver(), PETScFactory.instance())

        def solver_setup(self, A, P, problem, iteration):
            self.linear_solver().set_operator(A)

            PETScOptions.set("ksp_type", "preonly")
            PETScOptions.set("pc_type", "lu")
            PETScOptions.set("pc_factor_mat_solver_type", "mumps")

            self.linear_solver().set_from_options()

    parameters["ghost_mode"] = "shared_facet"
    parameters["refinement_algorithm"] = "plaza_with_parent_facets"

    parameters["form_compiler"]["quadrature_degree"] = 4
    parameters["std_out_all_processes"] = False
    parameters["ghost_mode"] = "shared_facet"

### Load measured temperature-time curves
    aa = np.loadtxt(pp.data_folder+pp.meas_fname)
    t_Tfile = aa[:,0]*1e-6-pp.t0_Tfile
    T_meas_pside_input, T_meas_oside_input = aa[:,1],aa[:,2]
    T0_p = T_meas_pside_input[0]
    T0_o = T_meas_oside_input[0] 
    def T_pside_fn(t):
        return np.interp(t,t_Tfile,T_meas_pside_input)
    def T_oside_fn(t):
        return np.interp(t,t_Tfile,T_meas_oside_input)
    def deltaT_pside(t):
        return T_pside_fn(t)-T0_p
    def deltaT_oside(t):
        return T_oside_fn(t)-T0_o

### Make mesh_fname.xdmf if it doesn't exist
    try:
        with open(pp.mesh_folder+pp.mesh_fname+'.xdmf', 'r') as fh:
            print(pp.mesh_fname+'.xdmf found')
    except FileNotFoundError:
        print(pp.mesh_fname +'.xdmf not found, so making it')
        pp_m = create_pp_mesh_from_pp(pp)
        make_diamond_mesh_2d(pp_m)

## Load params from mesh_fname.xdmf
    pp_m, mesh, xdmf_in = load_mesh(pp)

## Overwrite mesh_fname with new mesh if thicknesses don't match within 1%, then re-load mesh_fname
    if np.abs(pp_m.d_sam-pp.d_sam)/pp.d_sam > 0.01 or\
    np.abs(pp_m.d_coupler-pp.d_coupler)/pp.d_coupler > 0.01 or\
    np.abs(pp_m.d_ins_pside-pp.d_ins_pside)/pp.d_ins_pside > 0.01 or\
    np.abs(pp_m.d_ins_oside-pp.d_ins_oside)/pp.d_ins_oside > 0.01:
        print('Re-making '+ pp.mesh_fname +'.xdmf')
        pp_m = create_pp_mesh_from_pp(pp)
        make_diamond_mesh_2d(pp_m)
        pp_m, mesh, xdmf_in = load_mesh(pp)
        print('New mesh loaded, with d_sam = '+str(pp.d_sam))

#Cell function
    cf = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)
    xdmf_in.read(cf, "gmsh:physical")
    
    for j in range(0):
        markers = MeshFunction("bool", mesh, mesh.topology().dim(), True)
        mesh2 = refine(mesh, markers, redistribute=False)
        cf = adapt(cf, mesh2)
        mesh = mesh2

    dx = Measure("dx", subdomain_data=cf)
    x = SpatialCoordinate(mesh)
    r, z = x[0], x[1]

#Cell function for functionals (to not mess up FE model!!)
    cf_functional = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)

# Facet Function
    ff = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)
    ff_functional = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)

#Facets
    z1 = pp.d_coupler+pp.d_sam/2
    z2 = z1+pp.d_ins_pside+pp.d_diamond
    z3 = -z1 - pp.d_ins_oside - pp.d_diamond

    INTERIOR_BC_ID = 1
    CompiledSubDomain("(x[0] < r_coupler + DOLFIN_EPS) and near(x[1], z_coupler)",
                  z_coupler=z1, r_coupler = pp_m.r_coupler).mark(ff, INTERIOR_BC_ID)

    CentralSpot_rmax = Constant(0.25e-6)
    CentralSpotBndry = 5
    CompiledSubDomain(
        "(near(x[0], rmax)"
        "and (x[1] > zmin + DOLFIN_EPS)"
        "and (x[1] < zmax + DOLFIN_EPS))"
        "or (near(x[1], zmax)"
        "and (x[0] < rmax)"
        "or (near(x[1], zmin)"
        "and x[0] < rmax))",
        rmax = CentralSpot_rmax,zmax = z1, zmin=pp.d_sam/2).mark(ff_functional,CentralSpotBndry)

    EXT_ID = 7
    CompiledSubDomain("(near(x[0], r_diamond) or near(x[1], z2) or near(x[1], z3)) and on_boundary",\
                      r_diamond=pp_m.r_diamond, z2=z2,z3=z3).mark(ff,EXT_ID)


    steady_initial_state = True
    T_outerBC = 300.0

    polynomial_approximation_order = 1
    element_T = FiniteElement("CG", mesh.ufl_cell(), polynomial_approximation_order)
    V = FunctionSpace(mesh, element_T)
    v = TestFunction(V)
    T = interpolate(Constant(0.0), V)
    T.rename("T", "T")
    Tm = Function(V)
    Tm.vector()[:] = T.vector()
    Tth = Constant(0.5)*(T + Tm)

    filename2save = "T_"+pp.sim_fname

    info("Problem size: %d" % V.dim())

    class InternalTemperatureExpression(UserExpression):
         def __init__(self, **kwargs):
             self.t = 0.0
             super().__init__(**kwargs)

         def eval(self, values, x):
             r = x[0]
             RR = np.exp(-r**2/ (2*(pp.T_FWHM/2.35)**2))
             T_top = deltaT_pside(self.t)
             values[0] = T_outerBC + RR*T_top

         def value_shape(self):
             return ()

    internal_temperature_expression = InternalTemperatureExpression(degree = 0)

    DG0 = FunctionSpace(mesh, "DG", 0)     
    kappa_dg_0 = Function(DG0)
    kappa_dg_0.vector().set_local(np.fromiter(map(lambda v: pp.kappa[v], cf.array()), dtype=np.double))
          
#Boundary Conditions
    bc_outer = DirichletBC(V, Constant(T_outerBC), ff, EXT_ID)
    bc_inner = DirichletBC(V, internal_temperature_expression, ff, INTERIOR_BC_ID)
    bcs = [bc_outer, bc_inner]

    dS = Measure("dS", subdomain_data=ff)
    dS_functionals = Measure("dS", subdomain_data=ff_functional)
    ds = Measure("ds", subdomain_data=ff)
    dx_functionals = Measure("dx", subdomain_data=cf_functional)

    nn = FacetNormal(mesh)
    flux_form_CentralSpot = ((dot(kappa_dg_0*grad(T),nn))("+") + (dot(kappa_dg_0*grad(T),nn))("-"))*r*dS_functionals(CentralSpotBndry) #units: W/m/K* K/m * m * m = W 

### Steady state problem
    def form_steady_residual(u):
         residual = 0
         for mat in materials:
             residual += Constant(pp.kappa[mat]) * inner(grad(u), grad(v)) * r * dx(mat)
         Q = Constant(0.0)#power
         residual -= Q * v * r * dx(COUPLER)
         return residual

    if steady_initial_state:
         solve(form_steady_residual(T) == 0, T, bcs)

    residual = pp.dt*form_steady_residual(Tth)
    for mat in materials:
         residual += Constant(pp.rho[mat] * pp.cv[mat]) * (T - Tm) * v * r * dx(mat)

    jacobian = derivative(residual, T)

    xdmf = XDMFFile(pp.folder2save+filename2save+".xdmf")
    xdmf.parameters["functions_share_mesh"] = True
    xdmf.parameters["rewrite_function_mesh"] = True

    time_steps = []
    flux_CentralSpot = []
    T_monitor_pside = []
    T_monitor_oside = []
    T_meas_pside = []
    T_meas_oside = []
 
    XDMFFile("checkpoint.xdmf").write_checkpoint(T, "T", float(t)*1e6, append=False)
    xdmf.write(T, float(t)*1e6)

    problem = Problem(jacobian, residual, bcs)
    solver = CustomSolver()

    time_0 = 0.0
    n_t_steps = int(float(pp.time_f/pp.dt))
    for j in range(n_t_steps):
        info(" ".join(("Solving step %d" % j, "t=%.3e" % float(t), "dt=%.3e" % float(pp.dt))))

    ### Update time dependent problem variables
        Tm.vector()[:] = T.vector()
        t.assign(t+float(pp.dt))
        internal_temperature_expression.t = float(t)

    ### Solve the system and compute fluxes
        solver.solve(problem, T.vector())
        time_steps.append(float(t))
        flux_CentralSpot.append(assemble(flux_form_CentralSpot))
        T2add_p = utility.evaluate_function([[0.0, pp.d_sam/2.0+pp.d_coupler]], T)[0]
        T2add_o = utility.evaluate_function([[0.0, -(pp.d_sam/2.0 + pp.d_coupler)]], T)[0]
        T_monitor_pside.append(T2add_p)
        T_monitor_oside.append(T2add_o)
        #append T_meas_pside, oside
        T_meas_pside.append(T_pside_fn(float(t)))
        T_meas_oside.append(T_oside_fn(float(t)))
    ### Break if we've hit the final time
        if float(t) > pp.time_f:
             break

    ### Write temperature to file
        XDMFFile("checkpoint.xdmf").write_checkpoint(T, "T", float(t)*1e6, append=True)
        xdmf.write(T, float(t)*1e6)

    r_top_coupler = []
    final_T_top_coupler = []
    for r in np.linspace(0,pp_m.r_coupler,50):
        r_top_coupler.append(r)
        final_T_top_coupler.append(utility.evaluate_function([[r, pp.d_sam/2.0 + pp.d_coupler]],T)[0])

### Extract axial temperature distribution, ~ 1 insulation layer into diamond anvil
    z_axis = []
    final_T_z_axis = []
    z4 = pp.d_sam/2+pp.d_coupler
    for z in np.linspace(-z4-2*pp.d_ins_oside,z4+2*pp.d_ins_pside,200):
         z_axis.append(z)
         final_T_z_axis.append(utility.evaluate_function([[0.0, z]],T)[0])

    print('Saving '+filename2save + '.xdmf and .h5')

    if pp.time_delay_oside_meas != 0.0:
        def T_oside_sim_delayed(t):
            return np.interp(t+pp.time_delay_oside_meas,list(time_steps),list(T_monitor_oside))
        Tnew = []
        for tt in time_steps:
            Tnew.append(T_oside_sim_delayed(tt))
        T_monitor_oside = Tnew 

    Tp_sim_norm, To_sim_norm = normalize_Ts(T_monitor_pside,T_monitor_oside)
    Tp_meas_norm, To_meas_norm = normalize_Ts(T_meas_pside, T_meas_oside)

    xdmf.close()

    def save_FEM_results(fname):
        if MPI.rank(MPI.comm_world) == 0:
            import json
            with open(fname+".json", "w") as f:
                f.write(json.dumps({"time": time_steps,
                                "flux_CentralSpot": flux_CentralSpot,
                                "rho": pp.rho, "cv": pp.cv,
                                "kappa": pp.kappa, "T_FWHM": pp.T_FWHM, "d_sam": pp.d_sam, "d_coupler": pp.d_coupler,
                                "d_ins_pside": pp.d_ins_pside, "d_ins_oside": pp.d_ins_oside, "r_coupler": pp_m.r_coupler, "dt": float(pp.dt),
                                "T_pside_sim": T_monitor_pside, "T_oside_sim": T_monitor_oside,
                                "T_pside_meas": T_meas_pside, "T_oside_meas": T_meas_oside,
                                "r_top_coupler":r_top_coupler, "final_T_top_coupler":final_T_top_coupler,
                                "z_axis":z_axis, "final_T_z_axis":final_T_z_axis,
                                }))
        return

    save_FEM_results(pp.folder2save+pp.sim_fname)
    print(pp.sim_fname+'.json saved')
    print('Ksam = '+ str(pp.kappa[SAMPLE]))
    R2 = R_squared(To_meas_norm,To_sim_norm)
    rms = root_mean_square(To_meas_norm,To_sim_norm)
    return(rms,R2,time_steps,Tp_sim_norm,To_sim_norm,Tp_meas_norm,To_meas_norm,r_top_coupler,final_T_top_coupler,flux_CentralSpot)
################################

if __name__ == "__main__":

    pp = all_params(name2save = '80GPa',meas_fname = 'Pyro80GPa_29Jun2018_10us.txt', t0_Tfile = 0e-6, dt = Constant(0.2e-6), time_f = 7.5e-6,\
                              rho = {SAMPLE: 5164.0, COUPLER: 26504.0, INSULATOR: 4131.0, GASKET: 21000.0, DIAMOND: 3500.0},\
                              cv = {SAMPLE: 1158.0, COUPLER: 130.0, INSULATOR: 668.0, GASKET: 140.0, DIAMOND: 510.0},\
                              kappa = {SAMPLE: 3.8, COUPLER: 352.0, INSULATOR: 10.0, GASKET: 100.0, DIAMOND: 2000.0},\
                              T_FWHM = 13.2e-6, d_sam = 1.84e-6, d_coupler = 62e-9, d_ins_pside = 3.2e-6, d_ins_oside = 6.3e-6,\
                              r_sam = 20e-6, r_ins = 30e-6, r_diamond = 100e-6, d_diamond = 40e-6,
                              mesh_fname = 'diamond_mesh_2d_80GPa',sim_fname = 'guessed-fit_heatT_80GPa')
    run_heatT(pp)


