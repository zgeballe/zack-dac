'''
Use this file to run simulations via 'method Q' described in Geballe et al., EPSL 536 (2020) 116161. 

'Method Q' solves the heat equation with a source term at each side of a laser-heated diamond cell sample compressed between
layers of thermal insulation. One side, the pulsed-heated side, has a time-dependent source. The boundary condition is 
T = 300 K at the edges of the domain. This code solves the steady state problem first, and then the time-dependent problem using
adaptive time-stepping. Axial symmetry is assumed, enabling 2-d simulation.
'''   

import h5py #put this first
from dolfin import *
from dolfin_dg import *
import numpy as np
import json
import utility
from run_mesh_2d import make_diamond_mesh_2d
from scipy.optimize import curve_fit
from run_heatT import *

class all_heatQ_params:
    def __init__(self, p_FWHM,\
                    p0_pside, p0_oside, pscale_pulse, gauss_params2add,flux_fname,\
                    monitor_Planck_avgT = False, use_Q_from_heatT = True):
        self.p_FWHM = p_FWHM
        self.p0_pside = p0_pside
        self.p0_oside = p0_oside
        self.pscale_pulse = pscale_pulse
        self.gauss_params2add = gauss_params2add
        self.flux_fname = flux_fname
        self.monitor_Planck_avgT = monitor_Planck_avgT
        self.use_Q_from_heatT = use_Q_from_heatT

def gaussian(params,x):
    x0,FWHM, yscale,y0 = params
    return np.exp(-(x-x0)**2/(2*(FWHM/2.35)**2))*yscale+y0

def gaussian_truncated(params,x):
    x0,FWHM,x_trunc,yscale,y0 = params
    if x>x_trunc:
        return gaussian([x0,FWHM,yscale,y0],x)
    else:
        return 0

def greybody(wl_cal, *params):
    y = np.zeros_like(wl_cal)
    for i in range(0, len(params), 2):
        b = params[i]
        T = params[i+1]
        y = (b*3.67e19*wl_cal**-5)/(np.exp(1.4388e7/(wl_cal*T))-1)  #Blackbody
    return y

def blackbody(wl_b1, *params):
    wl_cal,b1 = wl_b1
    y = np.zeros_like(wl_cal)
    for i in range(0, len(params), 1):
        T = params[i]
        y = (b1*3.67e19*(wl_cal**-5)/(np.exp(1.4388e7/(wl_cal*T))-1))  #Blackbody
    return y

def run_heatQ(pp,pp_Q):
    print('Using '+ pp.mesh_fname + '.xdmf and .h5')
    t = Constant(0)  #Initializes time

    if pp_Q.use_Q_from_heatT:
        with open(pp.folder2save+pp_Q.flux_fname+".json", "r") as f:
            info = json.load(f)
        t_laser = np.array(info["time"], dtype=np.double)
        p_laser = np.array(info["flux_CentralSpot"], dtype=np.double)
    else:
        t_laser,p_laser,delta_t = laser_pulse_gaussians(pp.time_f)

    def power_fn(t):
        power = np.interp(t,t_laser,p_laser)
        if pp_Q.use_Q_from_heatT:
            power *= pp_Q.pscale_pulse # Scale p(t)
            power += gaussian_truncated(pp_Q.gauss_params2add,t) # Add gaussian to p(t)
            power = np.max(power,0) # Force p(t) > 0 
        return power

### Load measured temperature-time curves ##
    aa = np.loadtxt(pp.data_folder+pp.meas_fname)
    t_Tfile = aa[:,0]*1e-6-pp.t0_Tfile
    T_meas_pside_data, T_meas_oside_data = aa[:,1],aa[:,2]
    def T_pside_fn(t):
        return np.interp(t,t_Tfile,T_meas_pside_data)
    def T_oside_fn(t):
        return np.interp(t,t_Tfile,T_meas_oside_data)

## Make mesh_fname.xdmf if it doesn't exist
    try:
        with open(pp.mesh_folder+pp.mesh_fname+'.xdmf', 'r') as fh:
            print(pp.mesh_fname+'.xdmf found')
    except FileNotFoundError:
        print(pp.mesh_fname +'.xdmf not found, so making it')
        pp_m = create_pp_mesh_from_pp(pp)
        make_diamond_mesh_2d(pp_m)

## Load params from mesh_fname.xdmf
    pp_m, mesh, xdmf_in = load_mesh(pp)

## Overwrite mesh_fname with new mesh if thicknesses don't match within 1%, then re-load mesh_fname
    if np.abs(pp_m.d_sam-pp.d_sam)/pp.d_sam > 0.01:
        print('Re-making '+ pp.mesh_fname +'.xdmf')
        pp_m = create_pp_mesh_from_pp(pp)
        make_diamond_mesh_2d(pp_m)
        pp_m, mesh, xdmf_in = load_mesh(pp)
        print('New mesh loaded, with d_sam = '+str(pp.d_sam))

#Cell function
    cf = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)
    xdmf_in.read(cf, "gmsh:physical")
    
    for j in range(0):
        markers = MeshFunction("bool", mesh, mesh.topology().dim(), True)
        mesh2 = refine(mesh, markers, redistribute=False)
        cf = adapt(cf, mesh2)
        mesh = mesh2

    dx = Measure("dx", subdomain_data=cf)
    x = SpatialCoordinate(mesh)
    r, z = x[0], x[1]

#Cell function for functionals (to not mess up FE model!!)
    cf_functional = MeshFunction("size_t", mesh, mesh.topology().dim(), 0)

# Facet Function
    ff = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)
    ff_functional = MeshFunction("size_t", mesh, mesh.topology().dim()-1, 0)

#Facets
    z1 = pp.d_coupler+pp.d_sam/2
    z2 = z1 + pp.d_ins_pside + pp.d_diamond
    z3 = - z1 - pp.d_ins_oside - pp.d_diamond

    INTERIOR_BC_ID = 1
    CompiledSubDomain("(x[0] < r_coupler + DOLFIN_EPS) and near(x[1], z_coupler)",
                  z_coupler=z1, r_coupler = pp_m.r_coupler).mark(ff, INTERIOR_BC_ID)

    CentralSpot_rmax = Constant(0.25e-6)
    CentralSpotBndry = 5
    CompiledSubDomain(
        "(near(x[0], rmax)"
        "and (x[1] > zmin + DOLFIN_EPS)"
        "and (x[1] < zmax + DOLFIN_EPS))"
        "or (near(x[1], zmax)"
        "and (x[0] < rmax)"
        "or (near(x[1], zmin)"
        "and x[0] < rmax))",
        rmax = CentralSpot_rmax,zmax = z1, zmin=pp.d_sam/2).mark(ff_functional,CentralSpotBndry)

    EXT_ID = 7
    CompiledSubDomain("(near(x[0], r_diamond) or near(x[1], z2) or near(x[1], z3)) and on_boundary",\
                      r_diamond=pp_m.r_diamond, z2=z2,z3=z3).mark(ff,EXT_ID)

    sigma_laser = Constant(pp_Q.p_FWHM/2.35)

    steady_initial_state = True 
    p_time = Constant(0.0) #Watts, Will be reassigned to laser pulse after solving steady state                                                      
    p_top = (pp_Q.p0_pside+p_time)* Expression("x[1] > 0.0 ? mag : 0.0", mag=1.0, degree=0) #W                                                             
    p_bot = pp_Q.p0_oside*Expression("x[1] < 0.0 ? mag: 0.0", mag=1.0, degree=0) #W                                                                   
    laser_spot_normalisation = 1.0 / (sigma_laser ** 2 * 2 * float(pi) * 0.25e-6) #1/m^3, 2D gaussian normalisation                              
    power_density = (p_bot +  p_top) * exp(-r ** 2 / (2 * sigma_laser ** 2))*laser_spot_normalisation #W/m^3        

    polynomial_approximation_order = 1
    element_T = FiniteElement("CG", mesh.ufl_cell(), polynomial_approximation_order)
    V = FunctionSpace(mesh, element_T)
    v = TestFunction(V)
    T = interpolate(Constant(0.0), V)
    T.rename("T", "T")
    Tm = Function(V)
    Tm.vector()[:] = T.vector()
    Tth = Constant(0.5)*(T + Tm)

    filename2save = "T_"+pp.sim_fname

    print('Problem size',V.dim())

    class InternalTemperatureExpression(UserExpression):
         def __init__(self, **kwargs):
             self.t = 0.0
             super().__init__(**kwargs)

         def eval(self, values, x):
             r = x[0]
             RR = np.exp(-r**2/ (2*(T_FWHM/2.35)**2))
             T_top = deltaT_pside(self.t)
             values[0] = T_outerBC + RR*T_top

         def value_shape(self):
             return ()

    internal_temperature_expression = InternalTemperatureExpression(degree = 0)

    DG0 = FunctionSpace(mesh, "DG", 0)     
    kappa_dg_0 = Function(DG0)
    kappa_dg_0.vector().set_local(np.fromiter(map(lambda v: pp.kappa[v], cf.array()), dtype=np.double))
          
## Boundary conditions
    T_outerBC = 300.0
    bc_outer = DirichletBC(V, Constant(T_outerBC), ff, EXT_ID)
    bcs = [bc_outer]

    dS = Measure("dS", subdomain_data=ff)
    dS_functionals = Measure("dS", subdomain_data=ff_functional)
    ds = Measure("ds", subdomain_data=ff)
    dx_functionals = Measure("dx", subdomain_data=cf_functional)

    n = FacetNormal(mesh)
    flux_form_CentralSpot = ((dot(kappa_dg_0*grad(T),n))("+") + (dot(kappa_dg_0*grad(T),n))("-"))*r*dS_functionals(CentralSpotBndry)

####### Option to calculate planck-averaged_T #############
    if pp_Q.monitor_Planck_avgT:
        light_collection_radius = 6.25e-6
        yn = input("Calculating planck-averaged T over r = "+str(light_collection_radius*1e6)+" microns. Would you like to proceed (y/n)?")
        if yn !='y':
            print('Quitting...')
            quit()
        wavelengths = np.linspace(600,800,10) #nm
##############################################################


    def form_steady_residual(u):
# Steady problem
         residual = 0
         for mat in materials:
             residual += Constant(pp.kappa[mat]) * inner(grad(u), grad(v)) * r * dx(mat)
         Q = power_density #W/m^3 * p_top_normalisation
         residual -= Q * v * r * dx(COUPLER)
         return residual

    if steady_initial_state:
         solve(form_steady_residual(T) == 0, T, bcs)

    r_top_coupler = []
    initial_T_pside = []
    initial_T_oside = []
    r2save = np.linspace(0,pp_m.r_coupler,50)
    for rr in r2save:
        r_top_coupler.append(rr)
        initial_T_pside.append(utility.evaluate_function([[rr, pp.d_sam/2.0 + pp.d_coupler]],T)[0])
        initial_T_oside.append(utility.evaluate_function([[rr, -pp.d_sam/2.0 - pp.d_coupler]],T)[0])

    class DiamondProblem(PETScTSProblem):
        def F(self, u, u_t, u_tt):
            residual = form_steady_residual(u)
            time_part = 0
            for mat in materials:
                time_part += Constant(pp.rho[mat] * pp.cv[mat]) * u_t * v * r * dx(mat)
            return time_part + residual    

    xdmf = XDMFFile(pp.folder2save+filename2save+".xdmf")
    xdmf.parameters["functions_share_mesh"] = True
    xdmf.parameters["rewrite_function_mesh"] = True

    flux_CentralSpot = []#Constant(0.0)                                                                                                          
    time_steps = []
    power_used = []
    T_monitor_pside = []
    T_monitor_oside = []
    T_meas_pside = []
    T_meas_oside = []
    output_vec = Function(V)
    def monitor(ts, i, ts_t, x):
        t.assign(ts_t)
        p_now = power_fn(ts_t)
        p_time.assign(p_now) #W                                                                                         
        flux_CentralSpot.append(assemble(flux_form_CentralSpot))
        time_steps.append(ts_t)
        power_used.append(p_now)
        output_vec.vector()[:] = PETScVector(x)
        xdmf.write(output_vec, ts_t*1e6)
        if pp_Q.monitor_Planck_avgT:
            r2int = np.linspace(0,light_collection_radius,10)
            dr = r2int[1]-r2int[0]
            Ip =np.zeros(np.shape(wavelengths)); Io = np.zeros(np.shape(wavelengths))
            for i in np.r_[0:len(r2int)]:
                Tp = utility.evaluate_function([[r2int[i], pp.d_sam/2.0+pp.d_coupler]],T)[0]
                To = utility.evaluate_function([[r2int[i], -pp.d_sam/2.0-pp.d_coupler]],T)[0]
                Ip += greybody(wavelengths,*[1.,Tp])*r2int[i]*dr/np.pi/light_collection_radius**2
                Io += greybody(wavelengths,*[1.,To])*r2int[i]*dr/np.pi/light_collection_radius**2
            popt, pcov = curve_fit(greybody, wavelengths, Ip, p0=[1,Tp], maxfev=5000)
            T2add_p = popt[1]
            popt, pcov = curve_fit(greybody, wavelengths, Io, p0=[1,Tp], maxfev=5000)
            T2add_o= popt[1]
        else: 
            T2add_p = utility.evaluate_function([[0.0, pp.d_sam/2.0+pp.d_coupler]], T)[0]
            T2add_o = utility.evaluate_function([[0.0, -(pp.d_sam/2.0 + pp.d_coupler)]], T)[0]
        T_monitor_pside.append(T2add_p)
        T_monitor_oside.append(T2add_o)
        #append T_meas_pside, oside
        T_meas_pside.append(T_pside_fn(float(t)))
        T_meas_oside.append(T_oside_fn(float(t)))

    t = Constant(0.)#300e-9)#initialize                                                                                                          
    ts = PETScTSSolver(DiamondProblem(), T, bcs)

    PETScOptions.set("ksp_type", "preonly")
    PETScOptions.set("pc_type", "lu")
    PETScOptions.set("pc_factor_mat_solver_type", "mumps")
    PETScOptions.set("ts_monitor")
    PETScOptions.set("ts_type", "rosw")
    #PETScOptions.set("ts_rosw_type", "rodas3")                                                                                                  
    PETScOptions.set("ts_atol", 1e-8) #1e-8                                                                                                      
    PETScOptions.set("ts_rtol", 3e-5) #3e-5 is standard, 1e-5 slows simulation to reasonable speed. Zack added b.c. atol did not reduce timestep                   
    PETScOptions.set("ts_exact_final_time", "matchstep")
    ts.set_from_options()
    ts.ts.setMonitor(monitor)
    time_0 = 0.0
    #time_f = 10e-6                                                                                                                              
    ts.solve(t0=time_0, dt=pp.dt, max_t=pp.time_f, max_steps=300)

    final_T_top_coupler = []
    for rr in r2save:
        final_T_top_coupler.append(utility.evaluate_function([[rr, pp.d_sam/2.0 + pp.d_coupler]],T)[0])


## Extract axial temperature distribution, ~ 1 insulation layer into diamond anvil
    z_axis = []
    final_T_z_axis = []
    z4 = pp.d_sam/2+pp.d_coupler
    for z in np.linspace(-z4-2*pp.d_ins_oside,z4+2*pp.d_ins_pside,200):
         z_axis.append(z)
         final_T_z_axis.append(utility.evaluate_function([[0.0, z]],T)[0])

    print('Saving '+filename2save + '.xdmf and .h5')

    Tp_sim_norm, To_sim_norm = normalize_Ts(T_monitor_pside,T_monitor_oside)
    Tp_meas_norm, To_meas_norm = normalize_Ts(T_meas_pside, T_meas_oside)

    xdmf.close()

    def save_FEM_results(fname):
        if MPI.rank(MPI.comm_world) == 0:
            import json
            with open(fname+".json", "w") as f:
                f.write(json.dumps({"time": list(time_steps),
                                "flux_CentralSpot": flux_CentralSpot, "laser_power": list(power_used),
                                "rho": pp.rho, "cv": pp.cv,
                                "kappa": pp.kappa, "p_FWHM": pp_Q.p_FWHM, "d_sam": pp.d_sam, "d_coupler": pp.d_coupler,
                                "d_ins_pside": pp.d_ins_pside, "d_ins_oside": pp.d_ins_oside, "r_coupler": pp_m.r_coupler, "dt": 0,
                                "T_pside_sim": T_monitor_pside, "T_oside_sim": T_monitor_oside,
                                "T_pside_meas": T_meas_pside, "T_oside_meas": T_meas_oside,
                                "r_top_coupler":r_top_coupler, "final_T_top_coupler":final_T_top_coupler,
                                "initial_T_pside":initial_T_pside , "initial_T_oside": initial_T_oside,
                                "z_axis":z_axis, "final_T_z_axis":final_T_z_axis,
                                }))
        return

    save_FEM_results(pp.folder2save+pp.sim_fname)
    print(pp.sim_fname+'.json saved')
    
    R2 = R_squared(To_meas_norm,To_sim_norm)
    rms_oside_norm = root_mean_square(To_meas_norm,To_sim_norm)
    return(rms_oside_norm,R2,time_steps,T_monitor_pside,Tp_sim_norm,T_monitor_oside,To_sim_norm,T_meas_pside,Tp_meas_norm,T_meas_oside,\
     To_meas_norm,r_top_coupler,final_T_top_coupler,initial_T_pside, initial_T_oside,power_used)


if __name__ == "__main__":

    pp = all_params(name2save = '80GPa',meas_fname = 'Pyro80GPa_29Jun2018_10us.txt', t0_Tfile = 0e-6, dt = 1e-11, time_f = 7.5e-6,\
                              rho = {SAMPLE: 5164.0, COUPLER: 26504.0, INSULATOR: 4131.0, GASKET: 21000.0, DIAMOND: 3500.0},\
                              cv = {SAMPLE: 1158.0, COUPLER: 130.0, INSULATOR: 668.0, GASKET: 140.0, DIAMOND: 510.0},\
                              kappa = {SAMPLE: 3.75, COUPLER: 352.0, INSULATOR: 10.0, GASKET: 100.0, DIAMOND: 2000.0},\
                              d_sam = 1.84e-6, d_coupler = 62e-9, d_ins_pside = 3.2e-6, d_ins_oside = 6.3e-6,\
                              r_sam = 20e-6, r_ins = 30e-6, r_diamond = 100e-6, d_diamond = 40e-6, T_FWHM = 10e-6,\
                              mesh_fname = 'diamond_mesh_2d_80GPa',sim_fname = 'guessed-fit_heatQ_80GPa',\
                              )
    pp_Q = all_heatQ_params(p_FWHM = 9e-6, p0_pside = Constant(1.15), p0_oside = Constant(1.53), pscale_pulse = 1.15,\
                              gauss_params2add = [0, 3e-6, 5e-6, 0.03, 0],flux_fname = 'guessed-fit_heatT_80GPa')
                              
    run_heatQ(pp,pp_Q)


