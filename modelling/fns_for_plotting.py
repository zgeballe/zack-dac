'''
This file contains functions required for plot_heatT.py and plot_heatQ.py

'''

import numpy as np
import matplotlib.pyplot as plt 
import json
from run_heatQ import run_heatQ, all_heatQ_params, gaussian
from run_heatT import materials, SAMPLE, COUPLER, INSULATOR, GASKET, DIAMOND, normalize_Ts

def load_tT_json_heatT(pp):
    with open(pp.folder2save+pp.sim_fname+".json", "r") as f:
        info = json.load(f)
    tt_laser = np.array(info["time"], dtype=np.double)
    t_sim = tt_laser
    T_pside_sim = np.array(info["T_pside_sim"], dtype=np.double)
    T_oside_sim = np.array(info["T_oside_sim"], dtype=np.double)
    T_pside_meas = np.array(info["T_pside_meas"], dtype=np.double)
    T_oside_meas = np.array(info["T_oside_meas"], dtype=np.double)
    r_top_coupler = np.array(info["r_top_coupler"],dtype = np.double)
    T_top_coupler = np.array(info["final_T_top_coupler"],dtype = np.double)
    if np.char.find(pp.sim_fname,'heatQ') == -1:
        initial_T_pside = T_top_coupler
        initial_T_oside = T_top_coupler
    else:
        initial_T_pside = np.array(info["initial_T_pside"],dtype = np.double)
        initial_T_oside = np.array(info["initial_T_oside"],dtype = np.double)
    z_axis = np.array(info["z_axis"],dtype = np.double)
    T_z_axis = np.array(info["final_T_z_axis"],dtype = np.double)
    d_sam = info["d_sam"]
    d_coupler = info["d_coupler"]
    d_ins_pside = info["d_ins_pside"]
    d_ins_oside = info["d_ins_oside"]
    r_coupler = info["r_coupler"]
    kappa = info["kappa"]
    if pp.sim_fname[:17] == 'guessed-fit_heatT':
        Q = np.array(info["flux_CentralSpot"],dtype = np.double)        
    else:
        Q = np.array(info["laser_power"],dtype = np.double)
    tT_sim = (t_sim,T_pside_sim,T_oside_sim)
    tT_meas = (t_sim,T_pside_meas,T_oside_meas)
    tQ_sim = [t_sim,Q]
    rTp_sim = [r_top_coupler,initial_T_pside]
    rTo_sim = [r_top_coupler,initial_T_oside]
    return(kappa,tT_sim,tT_meas,tQ_sim, rTp_sim, rTo_sim)


def plot_tT(tT_sim,pp,normalize_boolean = False,fig_name = 'test',sim_color='r',tT_meas=[],fig_num = 1):
    fig = plt.figure(fig_num,figsize = (3.5,2.5))
    tt1 = np.array(tT_sim[0])*1e6
    Tp, To = tT_sim[1], tT_sim[2]
    if tT_meas != []:
        tt2 = np.array(tT_meas[0])*1e6
        Tp2, To2 = tT_meas[1],tT_meas[2]
    if normalize_boolean:
        plt.ylabel('Normalized temperature')
        Tp, To = normalize_Ts(Tp, To) 
        if tT_meas != []:
            Tp2, To2 = normalize_Ts(Tp2,To2)
    else:
        plt.ylabel('Temperature (K)')
    if tT_meas != []:
        plt.plot(tt2,Tp2,'ro')
        plt.plot(tt2,To2,'bo')
    plt.plot(tt1,Tp,'k')
    plt.plot(tt1,To,'k')
    plt.xlabel('Time ($\mu$s)')

    fig.tight_layout()
    fig.savefig(pp.folder2save+fig_name+'.pdf', bbxo_inches = 'tight', transparent = 'False')
    print('Saved '+fig_name+'.pdf')

def plot_tQ(tQ_sim,pp,fig_name = 'test',sim_color='k',tQ_meas=[],fig_num = 2):
    #if plt.fignum_exists(fig_num) and plt.ax: new_axis = 1
    #else: new_axis = 0
    if sim_color == 'g': new_axis = 1
    else: new_axis = 0
    fig = plt.figure(fig_num,figsize = (3.5,2.5))
    ax1 = plt.gca()    
    if new_axis:
        ax2 = ax1.twinx()
        ax2.set_ylabel('Power (W)', color=sim_color)  
        ax2.tick_params(axis='y', labelcolor=sim_color) 
    else: 
        ax1.set_ylabel('Power (W)', color=sim_color)  
        ax1.tick_params(axis='y', labelcolor=sim_color) 
    tt1 = np.array(tQ_sim[0])*1e6
    plt.plot(tt1,tQ_sim[1],sim_color)
    plt.xlabel('Time ($\mu$s)')
    if tQ_meas != []:
        tt2 = np.array(tQ_meas[0])*1e6
        plt.plot(tt2,tQ_meas[1],'k-o')
    fig.tight_layout()
    fig.savefig(pp.folder2save+fig_name+'.pdf', bbxo_inches = 'tight', transparent = 'False')
    print('Saved '+fig_name+'.pdf')

def plot_rT(rT_sim,pp,rT_files = [],fig_name = 'test', sim_color = 'k', fig_num = 3):
    fig = plt.figure(fig_num,figsize = (3.5,2.5))
    rr1 = np.array(rT_sim[0])*1e6
    TT1 = rT_sim[1]
    plt.plot(np.r_[np.flipud(-rr1),rr1],np.r_[np.flipud(TT1),TT1],sim_color)
    if rT_files != []:
          ### Load measured temperature-time curves ##
          aa = np.loadtxt(pp.data_folder + pp.meas_fname)
          tt = aa[:,0]-pp.t0_Tfile*1e6
          T_meas_pside_input, T_meas_oside_input = aa[:,1],aa[:,2]
          fig = plt.figure(2,figsize = (3.5,2.5))
          plt.plot(tt,T_meas_pside_input,'r.')#,mfc='none')
          plt.plot(tt,T_meas_oside_input,'b.')#,mfc = 'none')
        
          ## Plot r-T data 
          fig = plt.figure(4,figsize = (3.5,2.5))
          a1 = np.loadtxt(pp.data_folder+rT_files[0])
          a2 = np.loadtxt(pp.data_folder+rT_files[1])
          plt.plot(a1[:,0]*1e6, a1[:,1],'r.')
          plt.plot(a2[:,0]*1e6, a2[:,1],'b.')

    plt.xlabel('Distance ($\mu$m)')
    plt.ylabel('Temperature (K)')
    plt.xlim([-7,7])
    plt.ylim([1600,2500])
    fig.tight_layout()
    fig.savefig(pp.folder2save+fig_name+'.pdf', bbxo_inches = 'tight', transparent = 'False')

    print('Saved '+fig_name+'.pdf')


