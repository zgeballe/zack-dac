'''
Use this file to run and plot results of simulation using run_heatT.py, or to plot results from a previously run simulation. 

Define simulation parameters in pp.

Compare against laboratory data by defining pp.meas_fname as the filename containing the laboratory data.
'''

from run_heatT import normalize_Ts, run_heatT, materials, SAMPLE, COUPLER, INSULATOR, GASKET, DIAMOND, all_params
from fns_for_plotting import *

run_simulation = True

pp = all_params(name2save = '80GPa',meas_fname = 'Pyro80GPa_29Jun2018_10us.txt', t0_Tfile = 0e-6, dt = 0.2e-6, time_f = 7.5e-6,\
                          rho = {SAMPLE: 5164.0, COUPLER: 26504.0, INSULATOR: 4131.0, GASKET: 21000.0, DIAMOND: 3500.0},\
                          cv = {SAMPLE: 1158.0, COUPLER: 130.0, INSULATOR: 668.0, GASKET: 140.0, DIAMOND: 510.0},\
                          kappa = {SAMPLE: 3.8, COUPLER: 352.0, INSULATOR: 10.0, GASKET: 100.0, DIAMOND: 2000.0},\
                          T_FWHM = 13.2e-6, d_sam = 1.84e-6, d_coupler = 62e-9, d_ins_pside = 3.2e-6, d_ins_oside = 6.3e-6,\
                          r_sam = 20e-6, r_ins = 30e-6, r_diamond = 100e-6, d_diamond = 40e-6,
                          mesh_fname = 'diamond_mesh_2d_80GPa',sim_fname = 'guessed-fit_heatT_80GPa')

if run_simulation:
    rms,R2,tt,Tp_sim_norm,To_sim_norm,Tp_meas_norm,To_meas_norm,r_top_coupler,final_T_top_coupler,flux_Central_Spot = run_heatT(pp)
    tT_sim = (tt,Tp_sim_norm,To_sim_norm)
    tT_meas = (tt,Tp_meas_norm,To_meas_norm)
    tQ_sim = (tt,flux_Central_Spot)
else:
    print('Loading '+pp.sim_fname+'.json')
    kappa,tT_sim,tT_meas,tQ_sim, rTp_sim, rTo_sim =  load_tT_json_heatT(pp)

plot_tT(tT_sim, pp, normalize_boolean = 1, tT_meas=tT_meas, fig_name = 'tT_norm_heatT'+pp.name2save)
plot_tQ(tQ_sim, pp, tQ_meas=[],fig_name = 'tQ_heatT'+pp.name2save)

